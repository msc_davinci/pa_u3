package org.davinci.aspectodos;

public class ExceptionA extends Exception {
    private int detail;

    ExceptionA(int a){
        detail = a;
    }

    public String toString(){
        return " debido a la entrada del número [" + detail+"]";
    }
}
