package org.davinci.aspectodos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

public class Main {
    private static ArrayList<Integer> listaDeNumeros;

    static void compute(int a) throws ExceptionA{
        System.out.println("LLamado a compute[" + a + "]");
        if(a>10){
            throw new ExceptionA(a);
        }
    }

    static void computeb(int b) throws ExceptionB{
        System.out.println("LLamado a compute[" + b + "]");
        if(b>10){
            throw new ExceptionB(b);
        }
    }

    public static void main(String[] args) {
        Integer suma = 0;
        File file = new File("test.txt");
        FileInputStream fileInputStream = null;

        try {
            for (Integer numero: listaDeNumeros) { suma = suma + numero; }
            int a = 0;
            a = a +1;
            compute(1);
            compute(20);
            computeb(60);
            fileInputStream = new FileInputStream(file);
            fileInputStream.read();
        } catch (IOException e) {
            System.err.println("Error de Entrada");
        } catch (ExceptionB exceptionB) {
            System.err.println("Error de ExcepcionB" + exceptionB);
        } catch (ExceptionA exceptionA) {
            System.err.println("Error de ExcepcionA" + exceptionA);
        } catch (NullPointerException ne){
            System.err.println("Error de puntero nulo");
        }
    }
}
