package org.davinci.aspectotres;

public class HandledManyCharacters extends Exception{
    private String detail;

    HandledManyCharacters(String ex){
        detail = ex;
    }

    public String toString(){
        return " at debido a que contiene la entrada de la cadena {" + detail + "}";
    }
}
