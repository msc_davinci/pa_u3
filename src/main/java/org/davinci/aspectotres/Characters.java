package org.davinci.aspectotres;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class Characters {

    static void evaluarCadena(String cadena) throws HandledManyCharacters{
        System.out.println("Evaluando que no sea mayor a 30 caractéres ("+ cadena + ")");
        if(cadena.length()>30){
            throw new HandledManyCharacters(cadena);
        }
    }

    static void evaluarContieneNumeros(String cadena) throws HandledNumberException{
        System.out.println("Evaluando que no contenga números("+cadena+")");
        if(cadena.matches(".*\\d+.*")){
            throw new HandledNumberException(cadena);
        }
    }

    @DataProvider(name="words")
    Object[][] getDataFromDataProvider(){
        return new Object[][]{
                {"Ambos miramos al abismo,"},
                {"per0 cuando el abismo nos m1ró, "},
                {"tú parp@deast3"}
        };
    }

    @Test(dataProvider = "words")
    void evaluarFrase(String frase){
        try {
            evaluarCadena(frase);
            evaluarContieneNumeros(frase);
        }catch (HandledManyCharacters hmc){
            System.err.println("ERROR: la cadena contiene más de 30 caractéres" + hmc);
        }catch (HandledNumberException hne){
            System.err.println("ERROR: la cadena contiene números" + hne);
        }
    }
}
