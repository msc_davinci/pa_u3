package org.davinci.aspectotres;

public class HandledNumberException extends Exception {
    private String detail;

    HandledNumberException(String ex){
        detail = ex;
    }

    public String toString(){
        return " at debido a que contiene números {" + detail + "}";
    }
}
